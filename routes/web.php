<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\PageController;
use App\Http\Controllers\UsersController;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('login');
})->name('login');

Route::controller(AuthController::class)->group(function () {
    Route::post('/authentication', 'index');

    Route::get('/', 'loginPage')->name('login');
});


/**
 * middleware 'auth:sanctum' di gunakan untuk mencegah user yang belum login masuk
 * hanya user yang sudah login yang di beri akses masuk
 */
Route::middleware(['auth:sanctum', 'verified'])->group(function () {

Route::get('/dashboard', function () {
    return view('index');
});

Route::controller(AuthController::class)->group(function (){
    Route::get('/profile', 'profile');

    Route::get('/', 'logout')->name('logout');
});

Route::prefix('user')->group(function () {
    Route::get('', [UsersController::class, 'index']);
    Route::get('edit/{id}', [UsersController::class, 'edit']);
    Route::post('store', [UsersController::class, 'store']);
    Route::post('update', [UsersController::class, 'update']);
    Route::post('delete', [UsersController::class, 'delete']);
});

Route::prefix('category')->controller(CategoryController::class)->group(function () {
    Route::get('/', 'index');
    Route::get('/edit/{id}', 'edit');
    Route::post('/store', 'store');
    Route::post('/update', 'update');
    Route::post('/delete', 'delete');
});

Route::prefix('pages')
->controller(PageController::class)->group(function () {
    Route::get('/', 'index');
    Route::get('/edit/{id}', 'edit');
    Route::post('/store', 'store');
    Route::post('/update', 'update');
    Route::post('/delete', 'delete');
});

});