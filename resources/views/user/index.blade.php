<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Daftar Pengguna</title>

  <link 
    rel="stylesheet" type="text/css" 
    href="{{asset('custom/custom.css?ls=25071038')}}" />
</head>
<body>

<div class="card box-shadow radius-10px">

  <form action="{{url('user/store')}}" method="post">
    @csrf

    <x-partial.input
      label="Nama Depan" placeholder="Masukan nama depan"
      id="firstNameInput" name="first_name" />
    <x-partial.input
      label="Nama Belakang" placeholder="Masukan nama belakang"
      id="lastNameInput" name="last_name" />
    <x-partial.input
      label="Tanggal Lahir" type="date" 
      id="birthDateInput" name="birth_date" />
    <x-partial.input
      label="Email" placeholder="Masukan email" 
      id="emailInput" name="email" />
    <x-partial.input
      label="Password" placeholder="Masukan password"
      id="passwordInput" name="password" />

    <button type="submit">Simpan</button>

  </form>
  
  </div>

  <h4>Daftar Pengguna :</h4>
  <table>
    <thead>
      <tr>
        <td>Nama Depan</td>
        <td>Nama Belakang</td>
        <td>Tanggal Lahir</td>
        <td>Email</td>
        <td>Aksi</td>
      </tr>
    </thead>
    <tbody>
      @foreach ($collection as $item)
        <tr>
          <td>{{$item->first_name}}</td>
          <td>{{$item->last_name}}</td>
          <td>{{$item->birth_date}}</td>
          <td>{{$item->email}}</td>
          <td>
            <a href="{{url('user/edit/' . $item->id)}}">Edit</a>
            
            <x-partial.delete url="user/delete" id="{{$item->id}}" />
          </td>
        </tr>
      @endforeach
    </tbody>
  </table>

</body>
</html>