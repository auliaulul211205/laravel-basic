<?php

namespace App\Http\Controllers;

use App\Models\Page;
use Illuminate\Http\Request;
use InvalidArgumentException;
use Illuminate\Support\Facades\Validator;

class PageController extends Controller
{
    function index(Request $req){
        /*untuk mengambil data keseluruhan pada model page */
        $collection = Page::with('category')->orderBy('id', 'desc')->get();
    
        /*untuk menampung data dan di simpan ke array*/
        $data = [
            'collection' => $collection,
        ];

        return view('page.index', $data);
    }

    function edit($id){
        /* berfungsi untuk mencari data sesuai colum $id pada model page*/
        $find = Page::find($id);
         
        //jika data $find tidak di temukan 
        //hasil akan not found
        if (!$find) { 
            return abort(404);
        }

        return view('page.edit', [
            'item' => $find
        ]);
    }

    public function upload($img, $folder, $default = null)
    {
        try {
            /*jika default memiliki isi selain NULL, akan langsung melakukan 
            return $default dan tidak melanjutkan sampai bawah*/
            if (!$img && $default) {
                return $default;
            }

            $folder_ext = explode('/', $folder);
            $path = 'storage/upload/'.$folder;

            /* mengecek tersedia atau tidak folder pada $path*/
            if (!file_exists($path)) {
                mkdir($path, 0755, true);
            }

            $extension = $img->getClientOriginalExtension();

            /* untuk mencari nama pada file yang di upload */
            $file = rand(000000, 999999).'_'.$folder_ext[0].'.'.$extension;

            /* untuk mengupload file yang dipilih*/
            $path = $img->move('storage/upload/'.$folder, $file);

            return 'storage/upload/'.$folder.'/'.$file;
        } catch (\Throwable $th) {
            throw new InvalidArgumentException($th->getMessage(), 500);
        }
    }

    function deleteFile($img) {
        /*untuk mengecek apa file tersedia atau tidak di dalam folder */
        if(file_exists($img)){
            /* untuk menghapus file yang terdapat pada $img*/
          unlink($img);
        }
    }

    function store(Request $req){
        try {
            /* untuk memvalidasi inputan, sesuai dengan aturan di bawah*/
            $validator = Validator::make($req->all(), [
                'title' => 'required',
                'id_category' => 'required',
                'description' => 'required',
                'image' => 'required|image|max:2222'
            ]);
            /*jika terdapat inputan yang tidak sesuai aturan, fungsi akan dihentikan*/
            if ($validator->fails()) {
                throw new InvalidArgumentException($validator->errors()->first(), 422);
            }

            /**
             *  Page::create() untuk menyimpan data pada table, yang terdapat pada protected $table di model Page
             *  $req->only() hanya akan mengambil inputan yang dipilih di dalamnya
              */ 
            Page::create(array_merge(
                $req->only(['title', 'description', 'id_category']), 
                [
                    'image' => $this->upload($req->image, 'page')
                ]
            ));
            
            /* untuk melempar halaman yang dituju didalam redirect*/
            return redirect('/pages');
        } catch (\Throwable $th) {
            return response()->json([
                'code' => 500,
                'success' => false,
                'message' => $th->getMessage(),
                'line' => $th->getLine(),
                'file' => $th->getFile()
            ], 500);
        }
    }
    
    function update(Request $req){
        try {
            $validator = Validator::make($req->all(), [
                'id' => 'required',
                'title' => 'required',
                'id_category' => 'required',
                'description' => 'required',
                'image' => 'image|max:2222'
            ]);
            if ($validator->fails()) {
                throw new InvalidArgumentException($validator->errors()->first(), 422);
            }

            $find = Page::find($req->id);

            $imageOld = $find->image;

            if (!$find) {
                return abort(404);
            }

            /**
             * $find->update() untuk memperbarui pada tabel 
             * bisa di ubah dengan Page::find($req->id)->update(...)
             */
            $find->update(array_merge(
                $req->only(['title', 'description', 'id_category']),
                [
                    'image' => $this->upload($req->image, 'page', $find->image)
                ]
            ));

            /* untuk mengecek apa kita mengupload gambar atau tidak */
            if ($req->image) {
                /* jika melakukan upload gambar, gambar yang sebelumnya akan terhapus*/
                $this->deleteFile($imageOld);
            }
            
            return redirect('/pages');
        } catch (\Throwable $th) {
            return response()->json([
                'code' => 500,
                'success' => false,
                'message' => $th->getMessage(),
                'line' => $th->getLine(),
                'file' => $th->getFile()
            ], 500);
        }
    }

    function delete(Request $req){
        try {
            $find = Page::find($req->id);

            if (!$find) { return abort(404); }

            $imageOld = $find->image;

            /**
             * $find->delete() guna untuk menghapus data pada table 
             * bisa juga merubahnya dengan script lain seperti,
             * Page::find($id)->delete()
             */
            $find->delete();

            $this->deleteFile($imageOld);

            return redirect('/pages');
        } catch (\Throwable $th) {
            return response()->json([
                'code' => 500,
                'success' => false,
                'message' => $th->getMessage(),
                'line' => $th->getLine(),
                'file' => $th->getFile()
            ], 500);
        }
    }
}
